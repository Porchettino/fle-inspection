﻿namespace FLE_Initial_Inspection
{
    partial class M1_MENU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNext = new System.Windows.Forms.Button();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.RadioButton2 = new System.Windows.Forms.RadioButton();
            this.RadioButton4 = new System.Windows.Forms.RadioButton();
            this.RadioButton3 = new System.Windows.Forms.RadioButton();
            this.RadioButton1 = new System.Windows.Forms.RadioButton();
            this.Label10 = new System.Windows.Forms.Label();
            this.Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(194, 82);
            this.btnNext.Margin = new System.Windows.Forms.Padding(2);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(81, 35);
            this.btnNext.TabIndex = 7;
            this.btnNext.Text = "&Next";
            this.btnNext.UseVisualStyleBackColor = true;
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.Turquoise;
            this.Panel1.Controls.Add(this.RadioButton2);
            this.Panel1.Controls.Add(this.RadioButton4);
            this.Panel1.Controls.Add(this.RadioButton3);
            this.Panel1.Controls.Add(this.RadioButton1);
            this.Panel1.Controls.Add(this.btnNext);
            this.Panel1.Location = new System.Drawing.Point(11, 86);
            this.Panel1.Margin = new System.Windows.Forms.Padding(2);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(470, 135);
            this.Panel1.TabIndex = 22;
            // 
            // RadioButton2
            // 
            this.RadioButton2.AutoSize = true;
            this.RadioButton2.Location = new System.Drawing.Point(300, 46);
            this.RadioButton2.Margin = new System.Windows.Forms.Padding(2);
            this.RadioButton2.Name = "RadioButton2";
            this.RadioButton2.Size = new System.Drawing.Size(80, 17);
            this.RadioButton2.TabIndex = 8;
            this.RadioButton2.TabStop = true;
            this.RadioButton2.Tag = "4";
            this.RadioButton2.Text = "LED Check";
            this.RadioButton2.UseVisualStyleBackColor = true;
            // 
            // RadioButton4
            // 
            this.RadioButton4.AutoSize = true;
            this.RadioButton4.Location = new System.Drawing.Point(300, 24);
            this.RadioButton4.Margin = new System.Windows.Forms.Padding(2);
            this.RadioButton4.Name = "RadioButton4";
            this.RadioButton4.Size = new System.Drawing.Size(91, 17);
            this.RadioButton4.TabIndex = 3;
            this.RadioButton4.TabStop = true;
            this.RadioButton4.Tag = "3";
            this.RadioButton4.Text = "Output Check";
            this.RadioButton4.UseVisualStyleBackColor = true;
            // 
            // RadioButton3
            // 
            this.RadioButton3.AutoSize = true;
            this.RadioButton3.Location = new System.Drawing.Point(87, 46);
            this.RadioButton3.Margin = new System.Windows.Forms.Padding(2);
            this.RadioButton3.Name = "RadioButton3";
            this.RadioButton3.Size = new System.Drawing.Size(165, 17);
            this.RadioButton3.TabIndex = 2;
            this.RadioButton3.TabStop = true;
            this.RadioButton3.Tag = "2";
            this.RadioButton3.Text = "Flash Firmware and EEPROM";
            this.RadioButton3.UseVisualStyleBackColor = true;
            // 
            // RadioButton1
            // 
            this.RadioButton1.AutoSize = true;
            this.RadioButton1.Location = new System.Drawing.Point(87, 24);
            this.RadioButton1.Margin = new System.Windows.Forms.Padding(2);
            this.RadioButton1.Name = "RadioButton1";
            this.RadioButton1.Size = new System.Drawing.Size(88, 17);
            this.RadioButton1.TabIndex = 0;
            this.RadioButton1.TabStop = true;
            this.RadioButton1.Tag = "1";
            this.RadioButton1.Text = "Water Check";
            this.RadioButton1.UseVisualStyleBackColor = true;
            // 
            // Label10
            // 
            this.Label10.BackColor = System.Drawing.Color.Teal;
            this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Label10.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Label10.Location = new System.Drawing.Point(11, 9);
            this.Label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(470, 75);
            this.Label10.TabIndex = 21;
            this.Label10.Text = "Select Process";
            this.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // M1_MENU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 235);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.Label10);
            this.Name = "M1_MENU";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "M1_MENU";
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnNext;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.RadioButton RadioButton2;
        internal System.Windows.Forms.RadioButton RadioButton4;
        internal System.Windows.Forms.RadioButton RadioButton3;
        internal System.Windows.Forms.RadioButton RadioButton1;
        internal System.Windows.Forms.Label Label10;
    }
}